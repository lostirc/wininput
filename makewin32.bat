set /p OUTPUTPATH=Enter output path: %=%
set /p SRCPATH=Enter source path: %=%
set /p INTERFACEPATH=Enter as3 interface root path (C:\): %=%
set /p CLASSPATH=Enter interface package name relative from root path (dns.notation): %=%
set /p SWCNAME=Enter output swc name: %=%
set /p DESCPATH=Eneter descriptor xml path: %=%
set /p DLLPATH=Enter win32 dll path: %=%
set /p ANENAME=Enter ANE name: %=%
for %%F in (%DLLPATH%) do set "DLLNAME=%%~nxF"
set "NEWDLLPATH=%SRCPATH%\%DLLNAME%"
MOVE %DLLPATH% %NEWDLLPATH%
set "SWCOUTPUT=%SRCPATH%\%SWCNAME%"
set "TMPDIR=%SRCPATH%\tmp"
CALL md %TMPDIR%
CALL C:\sdks\flex\4.6\bin\acompc -source-path %INTERFACEPATH% -include-classes %CLASSPATH% -swf-version=14 -directory=true -output=%TMPDIR%
MOVE %TMPDIR%\library.swf %SRCPATH%\library.swf
rmdir /s /q %TMPDIR%
CALL C:\sdks\flex\4.6\bin\acompc -source-path %INTERFACEPATH% -include-classes %CLASSPATH% -swf-version=14 -output %SWCOUTPUT%
set "ANEPATH=%OUTPUTPATH%\%ANENAME%
CALL C:\sdks\flex\4.6\bin\adt -package -target ane %ANEPATH% %DESCPATH% -swc %SWCOUTPUT% -platform Windows-x86 -C %SRCPATH% %DLLNAME% library.swf