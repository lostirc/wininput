#include "KeyboardHelper.h"
#include <assert.h>


void KeyboardHelper::sendKey(std::string str, bool isUpeercase)
{
	//grab thg first input str
	const char * key = str.c_str();
	for (uint32_t i = 0 ; i < strlen(key); ++i){
		KeyboardHelper::doInputVk(key[i], false);
	}
	
}

void KeyboardHelper::sendWscanKey(std::string str)
{
	const char * key = str.c_str();
	for (uint32_t i = 0 ; i < strlen(key) ; ++i){
		KeyboardHelper::doInputwScan(key[i]);
	}

}

void KeyboardHelper::sendEnterKey()
{
	INPUT input = {0};
	input.type = INPUT_KEYBOARD;
	input.ki.time = 0;
	input.ki.dwExtraInfo = 0;

	input.ki.wVk = VK_RETURN;

	SendInput(1, &input, sizeof(INPUT));


}

void KeyboardHelper::doInputwScan(const char wordInput)
{
	const char vk = wordInput;
	INPUT input = {0};
	input.type = INPUT_KEYBOARD;
	input.ki.time = 0;
	input.ki.dwExtraInfo = 0;


	input.ki.wVk = 0;
	input.ki.dwFlags = KEYEVENTF_UNICODE;
	input.ki.wScan = wordInput;

	//debug
	//assert(false);
	SendInput(1, &input, sizeof(INPUT));
}

void KeyboardHelper::doInputVk(const char wordInput, bool isUppercase)
{
	const char vk = wordInput;
	INPUT input = {0};
	input.type = INPUT_KEYBOARD;
	input.ki.time = 0;
	input.ki.dwExtraInfo = 0;


	//(re)set dwFlags
	if ( (vk >= 33 && vk <= 46) || (vk >= 91 && vk <= 93) ) {
		input.ki.dwFlags = KEYEVENTF_EXTENDEDKEY;
	} else {

		if(isUppercase){ //send shift code
			input.ki.dwFlags = KEYEVENTF_EXTENDEDKEY;
			input.ki.wVk = VK_SHIFT;
			SendInput(1, &input, sizeof(INPUT));
		}

		input.ki.dwFlags = 0;
	}
	input.ki.wVk = 0;
	input.ki.dwFlags = KEYEVENTF_UNICODE;
	input.ki.wScan = wordInput;

	//debug
	//assert(false);
	SendInput(1, &input, sizeof(INPUT));

	input.ki.dwFlags = KEYEVENTF_KEYUP;
    SendInput(1, &input, sizeof(INPUT));

	if(isUppercase){ //release shift
		input.ki.dwFlags = KEYEVENTF_EXTENDEDKEY|KEYEVENTF_KEYUP;
        input.ki.wVk = VK_SHIFT;
        SendInput(1, &input, sizeof(INPUT));
	}
}