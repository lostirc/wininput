#ifndef MOUSEHELPER_H
#define MOUSEHELPER_H
#include <stdlib.h>
#include <Windows.h>
#include "FlashRuntimeExtensions.h"

class MouseHelper{


public:
	void moveMouseLocation(int32_t x,  int32_t y);
	void moveMouseToLocation(int32_t x, int32_t y);
	void doLeftClick();
	void doRightClick();

};
#endif
