#include "FlashRuntimeExtensions.h"
#include <Windows.h>
#include "MouseHelper.h"
#include "KeyboardHelper.h"

#ifdef __cplusplus
extern "C" {
#endif

	__declspec(dllexport) void ExtInitializer(void** extDataToSet, FREContextInitializer* ctxInitializerToSet, FREContextFinalizer* ctxFinalizerToSet);  
  
	__declspec(dllexport) void ExtFinalizer(void* extData);  
  
	__declspec(dllexport) FREObject moveMouse(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[]);  

	__declspec(dllexport) FREObject moveMouseTo(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[]);  

	__declspec(dllexport) FREObject leftClick(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[]);  
	
	__declspec(dllexport) FREObject rightClick(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[]);  
	
	__declspec(dllexport) FREObject inputKey(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[]);

	__declspec(dllexport) FREObject startKeyboardBinding(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[]);

	__declspec(dllexport) FREObject endKeyboardBinding(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[]);

	__declspec(dllexport) FREObject sendEnterKey(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[]);


#ifdef __cplusplus
}
#endif

