#ifndef KEYBOARDHELPER_H
#define KEYBOARDHELPER_H
#include <string>
#include <Windows.h>
#include "FlashRuntimeExtensions.h"

class KeyboardHelper{


public:
	//in
	void sendKey(std::string str, bool isUppercase);
	//in
	void sendWscanKey(std::string str);
	void sendEnterKey();
	void sendBackspace();
	void sendTab();
	void sendEsc();
	void sendSpace();
	void sendLeftArrow();
	void sendUpArrow();
	void sendRightArrow();
	void sendDownArrow();



private:
	void doInputVk(const char wordInput, bool isUppercase);
	void doInputwScan(const char wordInput);
	void doGenericInpit(WORD vk);
}; 

#endif