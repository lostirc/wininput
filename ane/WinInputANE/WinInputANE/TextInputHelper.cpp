#include "TextInputHelper.h"
#include <assert.h>
HWINEVENTHOOK evHook;

FREContext ctx;

void CALLBACK handleEventObjectFocus(HWINEVENTHOOK hook, DWORD evt, HWND hwnd,
									 LONG idObj, LONG idChild, DWORD thread, DWORD time);

void TextInputHelper::setupEventHook(FREContext iCtx)
{
	ctx = iCtx;
	CoInitialize(NULL);
	
	evHook = SetWinEventHook(EVENT_OBJECT_FOCUS, EVENT_OBJECT_END, NULL,
		handleEventObjectFocus, 0, 0,
		WINEVENT_OUTOFCONTEXT | WINEVENT_SKIPOWNPROCESS);
		
}

void TextInputHelper::releaseEventHook()
{

	UnhookWinEvent(evHook);
	CoInitialize(NULL);

}


void CALLBACK handleEventObjectFocus(HWINEVENTHOOK hook, DWORD evt, HWND hwnd,
									 LONG idObj, LONG idChild, DWORD thread, DWORD time)
{
	GUITHREADINFO threadInfo;
	threadInfo.cbSize = sizeof(GUITHREADINFO);

	BOOL hresult = GetGUIThreadInfo(thread, &threadInfo);

	if(hresult)
	{
		if(threadInfo.flags & GUI_CARETBLINKING)
		{
			
			FREDispatchStatusEventAsync(ctx, (const uint8_t *) "textFocus", (const uint8_t *) "focusIn");
		}
	}

}

//extern "C"   LRESULT _declspec(dllexport) __stdcall CALLBACK handleSetFocus(int nCode, WPARAM wParam, LPARAM lParam)
//{
//    if (nCode == HC_ACTION) {
//		CWPSTRUCT* info = (CWPSTRUCT*) lParam;
//		if(info->message == WM_SETFOCUS )
//		{
//			assert(FALSE);
//
//		}
//    }
//    return 1;
//}
//
//void TextInputHelper::setupEventHookEx(FREContext ctx)
//{
//	SetWindowsHookEx(WH_CALLWNDPROC, handleSetFocus, NULL, 0);
//}

