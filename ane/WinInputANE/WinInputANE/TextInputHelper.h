#ifndef TEXTINPUTHELPER_H
#define TEXTINPUTHELPER_U
#include <Windows.h>
#include "FlashRuntimeExtensions.h"

class TextInputHelper{

public:
	void setupEventHook(FREContext ctx);
	void releaseEventHook();

	void setupEventHookEx(FREContext ctx);

};

#endif