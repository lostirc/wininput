// TrakPadANE.cpp : Defines the exported functions for the DLL application.
//

#include "WinInputANE.h"
#include <stdlib.h>
#include "TextInputHelper.h"


extern "C"
{
	
	MouseHelper * mouseHelper;
	KeyboardHelper * keyboardHelper;
	TextInputHelper * textInputHelper;


	FREObject moveMouse(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])   
    {  
      
		int32_t x, y;
		FREObject result;  
		FREGetObjectAsInt32(argv[0], &x);  
		FREGetObjectAsInt32(argv[1], &y);
		mouseHelper->moveMouseLocation(x, y);	
		FRENewObjectFromInt32(1, &result);
	
		return result;  
    }

	FREObject moveMouseTo(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
	{
		
		int32_t x, y;
		FREGetObjectAsInt32(argv[0], &x);  
		FREGetObjectAsInt32(argv[1], &y);
		mouseHelper->moveMouseToLocation(x, y);	
		FREObject r;
		FRENewObjectFromInt32(1, &r);
		return r;
	}

	FREObject leftClick(FREContext ctx, void * funData, uint32_t argc, FREObject argv[])
	{
		mouseHelper->doLeftClick();
		FREObject r;
		FRENewObjectFromInt32(1, &r);
		return r;
	}

	FREObject rightClick(FREContext ctx, void * funData, uint32_t argc, FREObject argv[])
	{
		mouseHelper->doRightClick();
		FREObject r;
		FRENewObjectFromInt32(1, &r);
		return r;
	}

	FREObject inputKey(FREContext ctx, void * funData, uint32_t argc, FREObject argv[])
	{

		uint32_t len;
		const uint8_t * str;
		FREGetObjectAsUTF8(argv[0], &len, &str);
		//convert to std::string
		std::string s( reinterpret_cast<char const*>(str), len ) ;

		uint32_t isUppercase;
		FREGetObjectAsBool(argv[1], &isUppercase);
		uint32_t useWscan;
		FREGetObjectAsBool(argv[2], &useWscan);
		if((bool)useWscan){
			keyboardHelper->sendWscanKey(s);
		} else {
			keyboardHelper->sendKey(s, isUppercase);
		}
		FREObject r;
		FRENewObjectFromInt32(1, &r);
		return r;
	}


	FREObject sendEnterKey(FREContext ctx, void * funcData, uint32_t argc, FREObject argv[])
	{
		keyboardHelper->sendEnterKey();
		FREObject r;
		FRENewObjectFromInt32(1, &r);
		return r;
	}

	FREObject startKeyboardBinding(FREContext ctx, void * funData, uint32_t argc, FREObject argv[])
	{

		textInputHelper = new TextInputHelper;
		textInputHelper->setupEventHook(ctx);

		FREObject r;
		FRENewObjectFromInt32(1, &r);
		return r;
	}

	FREObject endKeyboardBinding(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
	{

		textInputHelper->releaseEventHook();
		delete textInputHelper;
		FREObject r;
		FRENewObjectFromInt32(1, &r);
		return r;
	}


	void ContextInitializer(void* extData, const uint8_t* ctxType, FREContext ctx, uint32_t* numFunctions, const FRENamedFunction** functions)  
    {  
		*numFunctions = 8;  
		FRENamedFunction* func = (FRENamedFunction*)malloc(sizeof(FRENamedFunction)*(*numFunctions));  
      

		func[0].name = (const uint8_t*)"moveMouse";  
		func[0].functionData = NULL;  
		func[0].function = &moveMouse;  

		func[1].name = (const uint8_t*)"leftClick";
		func[1].functionData = NULL;
		func[1].function = &leftClick;

		func[2].name = (const uint8_t*)"rightClick";
		func[2].functionData = NULL;
		func[2].function = &rightClick;

		func[3].name = (const uint8_t*)"inputKey";
		func[3].functionData = NULL;
		func[3].function = &inputKey;

		func[4].name = (const uint8_t*)"startKeyboardBinding";
		func[4].functionData = NULL;
		func[4].function = &startKeyboardBinding;

		func[5].name = (const uint8_t*)"endKeyboardBinding";
		func[5].functionData = NULL;
		func[5].function = &endKeyboardBinding;

		func[6].name = (const uint8_t*)"moveMouseTo";
		func[6].functionData = NULL;
		func[6].function = &moveMouseTo;

		func[7].name = (const uint8_t*)"sendEnterKey";
		func[7].functionData = NULL;
		func[7].function = &sendEnterKey;

		*functions = func;

    }  
      
    void ContextFinalizer(FREContext ctx)   
    {  
		mouseHelper = new MouseHelper;
		keyboardHelper = new KeyboardHelper;
		return;  
    } 


	void ExtInitializer(void** extData, FREContextInitializer* ctxInitializer, FREContextFinalizer* ctxFinalizer)   
	{  
		*ctxInitializer = &ContextInitializer;  
		*ctxFinalizer   = &ContextFinalizer;  
	}  
  
	void ExtFinalizer(void* extData)   
	{  
		delete mouseHelper;
		delete keyboardHelper;
		return;  
	}
	
}
