#include "MouseHelper.h"

	const double XSCALEFACTOR = 65535 / (GetSystemMetrics(SM_CXSCREEN) - 1);
	const double YSCALEFACTOR = 65535 / (GetSystemMetrics(SM_CYSCREEN) - 1);
 

void MouseHelper::moveMouseLocation(int32_t x, int32_t y)
{
	  

    // get current position
    POINT cursorPos;
    GetCursorPos(&cursorPos);
    double cx = cursorPos.x * XSCALEFACTOR;
    double cy = cursorPos.y * YSCALEFACTOR;

    double nx = (x) * XSCALEFACTOR;
    double ny = (y) * YSCALEFACTOR;

    INPUT input={0};
    input.type = INPUT_MOUSE;

	//move relative to current mouse location
	input.mi.dx = (LONG)nx + (LONG)cx;
	input.mi.dy = (LONG)ny + (LONG)cy;

    input.mi.dwFlags = MOUSEEVENTF_MOVE | MOUSEEVENTF_ABSOLUTE ;

    SendInput(1,&input,sizeof(INPUT));
}

void MouseHelper::moveMouseToLocation(int32_t x, int32_t y)
{


    const double nx = (x) * XSCALEFACTOR;
    const double ny = (y) * YSCALEFACTOR;

    INPUT input={0};
    input.type = INPUT_MOUSE;

	//move relative to current mouse location
	input.mi.dx = (LONG)nx;
	input.mi.dy = (LONG)ny;

    input.mi.dwFlags = MOUSEEVENTF_MOVE | MOUSEEVENTF_ABSOLUTE ;

    SendInput(1,&input,sizeof(INPUT));


}

void MouseHelper::doLeftClick()
{
	INPUT input = {0};
	input.mi.dwFlags = MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_LEFTDOWN;
	SendInput(1, &input, sizeof(input));
	Sleep(10);
	input.mi.dwFlags = MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_LEFTUP;
	SendInput(1, &input, sizeof(input));
}

void MouseHelper::doRightClick()
{
	INPUT input = {0};
	input.mi.dwFlags = MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_RIGHTDOWN;
	SendInput(1, &input, sizeof(input));
	Sleep(10);
	input.mi.dwFlags = MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_RIGHTUP;
	SendInput(1, &input, sizeof(input));
}
