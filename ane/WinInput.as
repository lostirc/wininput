package com.eyecu.WinInput.ane {
    import flash.events.EventDispatcher;
    import flash.external.ExtensionContext;
    import flash.events.*;

    public class WinInput extends EventDispatcher {
        private var _ctx:ExtensionContext;

        private var _WIN_TEXT_INPUT_FOCUS_IN:String = "winTextInputFocusIn";

        public function WinInput() {
            _ctx = ExtensionContext.createExtensionContext('com.eyecu.WinInput.ane.WinInput', '');
            _ctx.addEventListener(StatusEvent.STATUS, handleStatus);
        }

        public function get IN_TEXT_INPUT_FOCUS_IN():String {
            return _WIN_TEXT_INPUT_FOCUS_IN;
        }

        public function handleStatus(e:StatusEvent):void {
            dispatchEvent(new Event(_WIN_TEXT_INPUT_FOCUS_IN));
        }

        public function startKeyboardBinding():void {
            _ctx.call('startKeyboardBinding');
        }

        public function endKeyboardBinding():void {
            _ctx.call('endKeyboardBinding');
        }

        public function moveMouseTo(x:int, y:int):int {
            var result:int;
            result = _ctx.call('moveMouseTo', x, y) as int;
            return result;
        }

        public function moveMouse(x:int, y:int):int {
            var result:int;
            result = _ctx.call('moveMouse', x, y) as int;
            return result;
        }

        public function leftClick():Object {
            return _ctx.call("leftClick");
        }

        public function rightClick():Object {
            return _ctx.call("rightClick");
        }

        public function inputKey(key:String):Object {

            var alphaNumeric:RegExp = /^[a-zA-Z0-9]*$/;
            var inString:String;
            var uc:Boolean;
            //check for number
            if (!isNaN(Number(key))) {
                inString = key;
                uc = false;
            } else { //have a string check for case
                uc = key.charAt(0) == key.charAt(0).toUpperCase();
                inString = key.toUpperCase();
            }

            _ctx.call("inputKey", inString, uc, alphaNumeric.test(key));
            return inString;
        }

		public function sendEnterKey():Object {
            return _ctx.call("sendEnterKey");
        }


        public function dispose():void {
            _ctx.dispose();

        }
    }

}